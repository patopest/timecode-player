# Timecode Player

An app to Generate, Monitor and Play Timecode. Also a configuration frontend for the [Timecode Nixie Clock](https://gitlab.com/patopest/nixie-clock).  
Built using [Mithril.js](https://mithril.js.org/) and [Flowbite](https://flowbite.com/)/[TailwindCSS](https://tailwindcss.com/).   


## Features

- Wallclock generator/monitor.
- MTC monitor.


## Todo

- MTC Generator.
- LTC support with WebAudio.js.
- Nixie Clock configuration (via WebSerial ?).
- Player with audio track and timeline / playlist system.


## Dev

- Install dependencies

```shell
npm install
```

- Run App 

```shell
npm run start
```
This starts a Webpack development server available on http://localhost:3000/

- Build App

```shell
npm run build
```

- Run Production build app

```shell
npm run preview
```
App will be available at http://localhost:4000/


## References

- Mithril.js [docs](https://mithril.js.org/index.html).
- TailwindCSS [docs](https://tailwindcss.com/docs/installation).
- Flowbite [docs](https://flowbite.com/docs/getting-started/introduction/).
- Alexferl's [flowbite-mithril](https://github.com/alexferl/flowbite-mithril/tree/master).
- Flowbite [icons](https://flowbite.com/icons/).
- Midi Timecode [specification](https://www.midi.org/midi/specifications/midi1-specifications/midi-1-0-core-specifications/midi-time-code).
- Linear Timecode [specification](https://ieeexplore.ieee.org/document/7291029).
- Haute Technique's [apps](https://timecodesync.com/) as a source of inspiration.