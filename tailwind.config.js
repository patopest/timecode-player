module.exports = {
  content: [
    "./src/**/*.{html,js}",
    "./node_modules/flowbite-mithril/**/*.js"
  ],
  theme: {
    extend: {},
  },
  // darkMode: 'media',
  plugins: [
    require("flowbite/plugin")
  ],
};