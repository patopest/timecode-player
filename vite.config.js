import { resolve } from "path";
import { defineConfig } from "vite";

export default defineConfig({
  root: resolve(__dirname, "src"),
  publicDir: resolve(__dirname, "assets"),
  server: {
    port: 3000,
  },
  build: {
    outDir: resolve(__dirname, "dist"),
    emptyOutDir: true,
    sourceMap: true,
    css: {
      postcss: resolve(__dirname, "postcss.config.js"),
    },
    // rollupOptions: {
    //   // input: {
    //   //   app: resolve(__dirname, "src", "index.js"),
    //   // },
    //   input: resolve(__dirname, "src", "index.js"),
    //   // output: [
    //   //   {
    //   //     entryFileNames: "app.js",
    //   //   },
    //   // ],
    //   // output: {
    //   //   // entryFileNames: resolve(__dirname, "src", "index.js"),
    //   //   // file: "index.html"
    //   // }
    // },
  },
  preview: {
    port: 4000,
    open: true,
  },
});