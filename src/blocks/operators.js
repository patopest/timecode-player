import Timecode from "smpte-timecode";

import { Block } from "./block";


export class Operator extends Block {

    name = "Generic Operator";

    constructor(name) {
        super(name);
    }
}


export class SumOperator extends Operator {

    name = "Sum";

    constructor() {
        super("sum_operator");
        this.num_inputs = 2;
        this.inputs = [null, null];
    }

    update() {
        if (this.input_times[0] && this.input_times[1]) {
            this.timecode = new Timecode(this.input_times[0]).add(this.input_times[1]);
        }
        super.update();
    }
}


export class DiffOperator extends Operator {

    name = "Difference";

    constructor() {
        super("diff_operator");
        this.num_inputs = 2;
        this.inputs = [null, null];
    }

    update() {
        if (this.input_times[0] && this.input_times[1]) {
            this.timecode = new Timecode(this.input_times[0]).subtract(this.input_times[1]);
        }
        super.update();
    }
}


export class OffsetOperator extends Operator {

    name = "Offset";
    offset = null;
    direction = 0; // -1: negative, 1: positive

    constructor() {
        super("offset_operator");
        this.num_inputs = 1;
        this.inputs = [null];
        this.offset = new Timecode('00:00:00:00', this.framerate);
    }

    set_offset(direction, offset) {
        this.direction = direction;
        try {
            let new_offset = new Timecode(offset);
            this.offset = new_offset;
        }
        catch (err) {
            console.error(err);
        }
    }

    update() {
        if (this.input_times[0]) {
            this.timecode = new Timecode(this.input_times[0])
            try {
                if (this.direction == -1) {
                    this.timecode.subtract(this.offset);
                }
                if (this.direction == 1) {
                    this.timecode.add(this.offset);
                }
            }
            catch (error) {
                // console.error(error);
            }
        }
        super.update();
    }

    export() {
        let config = super.export();
        config.direction = this.direction;
        config.offset = this.offset.toString();
        return config;
    }

    import(config) {
        const {
            direction = this.direction,
            offset = this.offset,
            ...args
        } = config;

        super.import(config);
        this.set_offset(direction, offset);
    }
}

