import Timecode from "smpte-timecode";

import { Blocks } from "./index";

export class Block {

    id = "";
    name = "Generic Block";
    inputs = []; // store connected input nodes
    input_times = []; // store sent input nodes timecodes
    num_inputs = 1;
    outputs = []; // store connected output nodes
    num_outputs = 1; // 0 or 1 (means many)
    framerate = 30;
    timecode = null;
    // UI related stuff (but generic)
    on_change_cb = null;
    position = { x: 0, y: 0 };

    constructor(name) {
        this.id = name + "_" + generateUID();
        Blocks.push(this);
        console.log(this.id + ": created");
        this.timecode = new Timecode('00:00:00:00', this.framerate);
    }

    // Getters / setters
    get type() {
        return this.constructor.name;
    }

    toString() {
        return this.id;
    }

    set_position(x, y) {
        this.position.x = x;
        this.position.y = y;
    }

    // Connections
    connect_input(index, node) {
        if (index < this.num_inputs) {
            console.log("connecting ", node.toString(), "->", this.toString());
            if (this.inputs[index]) {
                this.disconnect_input(this.inputs[index]);
            }
            this.inputs[index] = node;
            node.connect_output(this); // forward connection to upstream node
        }
    }

    disconnect_input(node) {
        const index = this.inputs.indexOf(node);
        if (index > -1) {
            console.log("disconnecting ", node.toString(), "->", this.toString());
            this.inputs[index] = null;
            this.input_times[index] = null;
            node.disconnect_output(this); // forward connection to downstream node
        }
    }

    connect_output(node) {
        this.outputs.push(node);
    }

    disconnect_output(node) {
        const index = this.outputs.indexOf(node);
        if (index > -1) {
            this.outputs.splice(index, 1);
            node.disconnect_input(this);
        }
    }

    // Data flow / runtime
    input(node, value) {
        const index = this.inputs.indexOf(node);
        if (index > -1 && index < this.num_inputs) {
            this.input_times[index] = value;
        }
    }

    update() { // general logic to be run for all nodes
        this.output();

        if (this.on_change_cb) {
            this.on_change_cb();
        }
    }

    output() {
        // console.log(this.id + ": sending outputs to")
        for (const output of this.outputs) {
            output.input(this, this.timecode);
        }
    }

    on_change(callback) {
        this.on_change_cb = callback;
    }

    // Export / Import state
    export() {
        const config = {
            id: this.id,
            type: this.type,
            name: this.name,
            framerate: this.framerate,
            inputs: this.inputs.map((input) => input ? input.id : null),
            outputs: this.outputs.map((output) => output.id),
            position: this.position,
        }
        return config;
    }

    import(config) {
        const {
            id = this.id,
            name = this.name,
            framerate = this.framerate,
            position = this.position,
            ... args
        } = config;

        this.id = id;
        this.name = name;
        this.framerate = framerate;
        this.position = position;
    }

    delete() {
        for (const input of this.inputs) {
            this.disconnect_input(input);
        }
        for (const output of this.outputs) {
            this.disconnect_output(output);
        }

        let index = Blocks.indexOf(this);
        if (index > -1) {
            Blocks.splice(index, 1);
        }
    }
}


// Function from https://stackoverflow.com/a/6248722
function generateUID() {
    var firstPart = (Math.random() * 46656) | 0;
    var secondPart = (Math.random() * 46656) | 0;
    firstPart = ("000" + firstPart.toString(36)).slice(-3);
    secondPart = ("000" + secondPart.toString(36)).slice(-3);
    return firstPart + secondPart;
}

