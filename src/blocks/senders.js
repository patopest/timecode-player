import Timecode from "smpte-timecode";

import { Block } from "./block";
import { MTC } from "../lib/mtc";
import { LTC } from "../lib/ltc";


export class Sender extends Block {

    name = "Generic Sender";
    device = null;
    num_outputs = 0;

    constructor(name) {
        super(name);
        this.num_inputs = 1;
        this.inputs = [null];
    }

    export() {
        let config = super.export();
        config.device = this.device;
        return config;
    }

    import(config) {
        super.import(config);
        if (config.device) {
            this.select_device(config.device);
        }
    }
}


export class MTCSender extends Sender {

    name = "MTC Sender";

    constructor() {
        super("mtc_sender");
        this.device = null;
    }

    select_device(device) {
        this.device = device;
    }

    list_devices() {
        return MTC.get_output_devices();
    }

    update() {
        if (this.input_times[0]) {
            this.timecode = this.input_times[0];
            this.framerate = this.timecode.frameRate;
            this.input_times.fill(null);
        }
        super.update();
    }

    // Special implementation to send to device
    output() {
        MTC.send(this.device, this.timecode);    
    }
}


export class LTCSender extends Sender {

    name = "LTC Sender";

    constructor() {
        super("ltc_sender");
        this.device = null;
    }

    select_device(device) {
        if (this.device) {
            LTC.unselect_output_device(this.device);
        }
        this.device = device;

        LTC.setup_output(this.device);
    }

    list_devices() {
        return LTC.get_output_devices();
    }

    update() {
        if (this.input_times[0]) {
            this.timecode = this.input_times[0];
            this.framerate = this.timecode.frameRate;
            this.input_times.fill(null);
        }
        super.update();
    }

    // Special implementation to send to device
    output() {
        LTC.send(this.device, this.timecode);    
    }

    delete() {
        super.delete()
        if (this.device) {
            LTC.unselect_output_device(this.device);
        }
    }
}
