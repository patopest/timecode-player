export * from "./block";
export * from "./receivers";
export * from "./operators";
export * from "./senders";


import { Block } from "./block";
import { MTCReceiver, LTCReceiver } from "./receivers";
import { SumOperator, DiffOperator, OffsetOperator } from "./operators";
import { MTCSender, LTCSender } from "./senders";


// Globals
export const Blocks = [];
export const BlockTypes = {
    "MTCReceiver":      MTCReceiver,
    "LTCReceiver":      LTCReceiver,
    "SumOperator":      SumOperator,
    "DiffOperator":     DiffOperator,
    "OffsetOperator":   OffsetOperator,
    "MTCSender":        MTCSender,
    "LTCSender":        LTCSender,
}


export function ExportBlocks() {

    let config = [];
    Blocks.forEach((block) => {
        let c = block.export();
        config.push(c);
    });

    return config;
};


export function ImportBlocks(config) {

    // Empty array
    Blocks.splice(0, Blocks.length);

    // Re-create the blocks
    config.forEach((c) => {
        let block = new BlockTypes[c.type]();
        block.import(c);
    });

    // Re-create connections
    config.forEach((c, i) => {
        let block = Blocks[i];
        c.inputs.forEach((input, i) => {
            let node = Blocks.find((b) => b.id == input);
            if (node) {
                block.connect_input(i, node);
            }
        })
    })

};