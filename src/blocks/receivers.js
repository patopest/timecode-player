import Timecode from "smpte-timecode";

import { Block } from "./block";
import { MTC } from "../lib/mtc";
import { LTC } from "../lib/ltc";


export class Receiver extends Block {

    name = "Generic Receiver";
    device = null;

    constructor(name) {
        super(name);
        this.num_inputs = 0;
        this.inputs = [];
    }

    // special implementation to bypass input node check
    input(node, value) {
        this.input_times[0] = value;
    }

    export() {
        let config = super.export();
        config.device = this.device;
        return config;
    }

    import(config) {
        super.import(config);
        if (config.device) {
            this.select_device(config.device);
        }
    }
}


export class MTCReceiver extends Receiver {

    name = "MTC Receiver";

    constructor() {
        super("mtc_receiver");
        this.device = null;
    }

    select_device(device) {
        if (this.device) {
            MTC.unselect_device(this.device);
            this.timecode = new Timecode('00:00:00:00', this.framerate);
        }
        this.device = device;

        MTC.read(device, this.input.bind(this));
    }

    list_devices() {
        return MTC.get_input_devices();
    }

    update() {
        if (this.input_times[0]) {
            this.timecode = this.input_times[0];
            this.framerate = this.timecode.frameRate;
            this.input_times.fill(null);
        }
        super.update();
    }

    delete() {
        super.delete()
        if (this.device) {
            MTC.unselect_device(this.device);
        }
    }
}


export class LTCReceiver extends Receiver {

    name = "LTC Receiver";

    constructor() {
        super("ltc_receiver");
        this.device = null;
    }

    select_device(device) {
        if (this.device) {
            LTC.unselect_input_device(this.device);
            this.timecode = new Timecode('00:00:00:00', this.framerate);
        }
        this.device = device;

        LTC.read(device, this.input.bind(this));
    }

    list_devices() {
        return LTC.get_input_devices();
    }

    update() {
        if (this.input_times[0]) {
            this.timecode = this.input_times[0];
            this.framerate = this.timecode.frameRate;
            this.input_times.fill(null)
        }
        super.update();
    }

    delete() {
        super.delete()
        if (this.device) {
            LTC.unselect_input_device(this.device);
        }
    }
}