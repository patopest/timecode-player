import m from "mithril";
import { App } from "./app";
import { localStore } from "./lib/storage";


const theme = localStore('theme');
// const mountNode = document.querySelector("#app");
// m.mount(document.body, App);
// m.render(document.body, "hello world");

m.route(document.body, "/", {
  "/": {
    render: function() {
      return m(App, { pane: "main"})
    },
  },
  "/manager": {
    render: function() {
      return m(App, { pane: "manager"})
    },
  },
  "/settings": {
    render: function() {
      return m(App, { pane: "settings"})
    },
  }
});



// Code from Tailwind docs to manually change theme
// On page load or when changing themes, best to add inline in `head` to avoid FOUC
if (window.matchMedia('(prefers-color-scheme: dark)').matches) {
  document.documentElement.classList.add('dark')
  theme.set('dark');
} else {
  document.documentElement.classList.remove('dark')
  theme.set('light');
}

// // Whenever the user explicitly chooses light mode
// localStorage.theme = 'light'

// // Whenever the user explicitly chooses dark mode
// localStorage.theme = 'dark'

// // Whenever the user explicitly chooses to respect the OS preference
// localStorage.removeItem('theme')