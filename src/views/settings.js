import m from "mithril";
import { Dropdown, Toggle, Card, Button, Table, Checkbox } from "flowbite-mithril";

import { MTC } from "../lib/mtc";
import { LTC } from "../lib/ltc";
import { Serial } from "../lib/serial";
import { Settings } from "../models/settings";
import { ExportBlocks, ImportBlocks } from "../blocks";


function toggleMidi() {

  console.log("Toggle midi");
  if (Settings.midi_enabled) {
    Settings.disable_midi();
  }
  else {
    Settings.enable_midi();
  }
}


function toggleAudio() {

  console.log("Toggle Audio");
  if (Settings.audio_enabled) {
    Settings.disable_audio();
  }
  else {
    Settings.enable_audio();
  }

}


function toggleSerial() {

  console.log("Toggle serial");

}


function toggleLabels() {

  console.log("Toggle Timecode Labels");
  if (Settings.show_labels) {
    Settings.set_labels(false);
  }
  else {
    Settings.set_labels(true);
  }
}


function exportBlocks() {
  console.log("Exporting blocks config to local storage");
  let config = ExportBlocks();
  Settings.set_blocks_config(config);
}


function importBlocks() {
  console.log("Importing blocks config from local storage");
  let config = Settings.get_blocks_config();
  if (config) {
    ImportBlocks(config);
  }
}


const SettingsCard = {
  view({ attrs, children }) {
    const { title, ...props } = attrs;

    return m("div", { class: "grid justify-center justify-items-center place-items-center gap-2"},
      m("h2", {}, title),
      m(Card, { class: "w-[32rem] gap-y-2", ...props }, children),
    )
  }
}


export const SettingsPanels = {

  oncreate: () => {
    MTC.list_devices();
    LTC.list_devices();
  },

  view: function(vnode) {
    const midi_devices = MTC.get_devices();
    const audio_devices = LTC.get_devices();
    const serial_device = Serial.get_device();

    return m("div", { class: "grid grid-flow-row auto-rows-max justify-center place-items-center my-4 gap-3" },
      [
        m(SettingsCard, { title: "Project" },
          m(Button, { onclick: () => { exportBlocks() }}, "Export"),
          m(Button, { onclick: () => { importBlocks() }}, "Import"),
        ),
        m(SettingsCard, { title: "Timecode" },
          m("div", { class: "flex justify-between items-center gap-x-3" },
            m("h3", "Default Framerate"),
            m(Dropdown, { id: "fps", label: `${Settings.default_framerate} FPS`, ignoreClickOutsideClass: false, trigger: "hover"},
              [
                m(Dropdown.Item, { onclick: () => { Settings.set_framerate(30) } },     "30 FPS"),
                m(Dropdown.Item, { onclick: () => { Settings.set_framerate(29.97) } },  "29.97 DF"),
                m(Dropdown.Item, { onclick: () => { Settings.set_framerate(25) } },     "25 FPS"),
                m(Dropdown.Item, { onclick: () => { Settings.set_framerate(24) } },     "24 FPS"),
              ],
            ),
          ),
          m(Toggle, { onclick: toggleLabels, checked: Settings.show_labels, label: "Show Number Labels" }),
        ),
        m(SettingsCard, { title: "MIDI" },
          m(Toggle, { onclick: toggleMidi, checked: Settings.midi_enabled, disabled: !MTC.supported(), label: "Enable MIDI" }),
          m(Table,
            m(Table.Head,
              m(Table.HeadCell, "Devices"),
              m(Table.HeadCell, "Input"),
              m(Table.HeadCell, "Output"),
            ),
            m(Table.Body,
              Object.entries(midi_devices).map((device) => {
                const [name, properties] = device;
                return [m(Table.Row,
                  m(Table.Cell, name),
                  m(Table.Cell, m(Checkbox, { id: "input",  disabled: true, checked: properties.input })),
                  m(Table.Cell, m(Checkbox, { id: "output", disabled: true, checked: properties.output })),
                ),];
              }),
            ),
          ),
        ),
        m(SettingsCard, { title: "Audio" },
          m(Toggle, { onclick: toggleAudio, checked: Settings.audio_enabled, disabled: !LTC.supported(), label: "Enable Audio" }),
          m(Table, { shadow: true},
            m(Table.Head,
              m(Table.HeadCell, "Devices"),
              m(Table.HeadCell, "Input"),
              m(Table.HeadCell, "Output"),
            ),
            m(Table.Body,
              Object.entries(audio_devices).map((device) => {
                const [name, properties] = device;
                return [m(Table.Row,
                  m(Table.Cell, name),
                  m(Table.Cell, m(Checkbox, { id: "input",  disabled: true, checked: properties.input })),
                  m(Table.Cell, m(Checkbox, { id: "output", disabled: true, checked: properties.output })),
                ),];
              }),
            ),
          ),
        ),
        m(SettingsCard, { title: "Serial" },
          m(Toggle, { onclick: toggleSerial, checked: Serial.enabled(), disabled: !Serial.supported(), label: "Enable Serial" }),
          m(Button, { onclick: () => { Serial.select_device() }, disabled: !Serial.supported() },
            serial_device ? "Connected" : "Connect"
          ),
        ),
      ]
    )
  }
}