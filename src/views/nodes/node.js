import m from "mithril";
import { Card, Dropdown } from "flowbite-mithril";
import { AdjustmentsHorizontalIcon, TrashBinIcon, ChevronDownIcon } from "flowbite-icons-mithril/solid";

import { Clock } from "../clock";


export const Node = {

  active: false,
  current: { x: 0, y: 0 },
  initial: { x: 0, y: 0 },
  offset: { x: 0, y: 0 },

  width: 400, // Define statically (in px)
  height: 200,

  oninit({ attrs }) {
    const { block, key, ...props } = attrs;

    this.id = key;
    this.block = block;
    this.current = {
      x: this.block.position.x,
      y: this.block.position.y,
    }
    this.offset = {
      x: this.block.position.x,
      y: this.block.position.y,
    }
  },

  oncreate({ attrs }) {
    this.block.on_change(() => {
      m.redraw();
    })
  },

  view({ attrs, children }) {
    const { onConnectorClick = null, ...props } = attrs;

    const onDragStart = (event) => {
      if (event.type === "touchstart") {
        this.initial.x = event.touches[0].clientX - this.offset.x;
        this.initial.y = event.touches[0].clientY - this.offset.y;
      }
      else {
        this.initial.x = event.clientX - this.offset.x;
        this.initial.y = event.clientY - this.offset.y;
        this.active = true;
      }
      // console.log("drag", this.initial);
    }

    const onDragEnd = (event) => {
      this.initial.x = this.current.x;
      this.initial.y = this.current.y;
      this.active = false;
      // console.log("end", this.initial);
    }

    const onDrag = (event) => {
      if (this.active) {
        event.preventDefault();
        // event.stopImmediatePropagation(); // not sure if necessary

        if (event.type === "touchmove") {
          this.current.x = event.touches[0].clientX - this.initial.x;
          this.current.y = event.touches[0].clientY - this.initial.y;
        }
        else {
          this.current.x = event.clientX - this.initial.x;
          this.current.y = event.clientY - this.initial.y;
        }

        this.offset.x = this.current.x;
        this.offset.y = this.current.y;

        this.block.set_position(this.current.x, this.current.y);
      }
    }

    let transform = "transform: translate3d(" + this.current.x + "px, " + this.current.y + "px, 0);";
    let z_index = " z-20";
    if (this.active) z_index = " z-30";

    return m("node",
      // Node
      m(Card, {
          id: this.block.id,
          data_element_type: "Node",
          class: "absolute flex-none top-0 w-[400px] h-[200px] active:ring-2 active:ring-gray-500" + z_index,
          style: transform + " container-type: inline-size;",
          onmousedown: onDragStart,
          onmouseup: onDragEnd,
          onmousemove: onDrag,
          // ontouchstart: onDragStart,
          // ontouchend: onDragEnd,
          // ontouchmove: onDrag,
        },
        m("div", { class: "flex flex-none gap-3 px-2 justify-between text-center items-center"}, 
          m("h1", { class: "text-xl" }, this.block.name),
          // children are only the specific controls in the top bar of the car
          children,
          m(NodeDropdown, { block: this.block }),
        ),
        m(Clock, {time: this.block.timecode }),
      ),
      // Connectors
      m(Connectors, { block: this.block, width: this.width, height: this.height, z_index: z_index, onConnectorClick }),
    );
  }
}


const NodeDropdown = {
  block: null,

  oninit({ attrs }) {
    const { block, ...props } = attrs;

    this.block = block;
  },

  view({ attrs }) {
    let hasDevice = "device" in this.block;
    let isOffsetOP = this.block.type === "OffsetOperator";
    let selected_device = this.block.device;

    const onDelete = () => {
      this.block.delete();
    }

    return m(Dropdown, { id: this.block.id+"dropdown", inline: true, class: "z-50", color: "gray", arrowIcon: false, label: m(AdjustmentsHorizontalIcon) },
      hasDevice &&
        m(Dropdown.Item, { icon: ChevronDownIcon },
          m(Dropdown, { id: this.block.id+"-deviceselect", inline: true, trigger: "hover", label: "Device"},
            this.block.list_devices().map(device => [
              m(Dropdown.Item,
                {
                  onclick: () => { this.block.select_device(device) },
                  class: selected_device == device ? " bg-gray-200 dark:bg-gray-500": "",
                }, 
                device),
            ]),
          ),
        ),
      isOffsetOP &&
        m(Dropdown.Item, { icon: TimerIcon },
          // m("text", { class: "mr-2" }, "Offset"),
          m(OffsetEditor, { block: this.block }),
        ),
      m(Dropdown.Item, { onclick: onDelete, icon: TrashBinIcon }, "Delete"),
    );
  }
}


const OffsetEditor = {

  block: null,
  offset: "",

  oninit({ attrs }) {
    const { block, ...props } = attrs;

    this.block = block;
    if (this.block.type === "OffsetOperator") {
      this.offset = this.block.direction < 0 ? "-" : "+";
      this.offset += this.block.offset.toString();
    }
  },

  view({ attrs }) {
    const { block, ...props } = attrs;

    const onOffsetInput = (event) => {
      this.offset = event.target.textContent;
    }

    const onOffsetEnter = (event) => {
      let update = false;
      if (event instanceof KeyboardEvent && event.key == "Enter") {
        update = true;
      }
      else if (event instanceof FocusEvent && event.type === "focusout") {
        update = true;
      }

      if (update) {
        let direction = 1;
        let offset = this.offset
        if (offset[0] === "-") {
          direction = -1;
          offset = offset.substring(1);
        }
        else if (offset[0] === "+") {
          direction = 1;
          offset = offset.substring(1);
        }
        this.block.set_offset(direction, offset);
        this.offset = this.block.direction < 0 ? "-" : "+";
        this.offset += this.block.offset.toString();
      }
    }

    return m("div", {
        contenteditable: true,
        oninput: onOffsetInput,
        onkeyup: onOffsetEnter,
        onfocusout: onOffsetEnter,
        class: "px-2",
        ...props
      },
      m.trust(this.offset)
    );
  }

}


const TimerIcon = {
  view() {
    return m("svg", {
        viewBox: "0 0 24 24",
        fill: "currentColor",
        class: "mr-2 h-5 w-5",
      },
      m("path", {
        d: "M17.6177 5.9681L19.0711 4.51472L20.4853 5.92893L19.0319 7.38231C20.2635 8.92199 21 10.875 21 13C21 17.9706 16.9706 22 12 22C7.02944 22 3 17.9706 3 13C3 8.02944 7.02944 4 12 4C14.125 4 16.078 4.73647 17.6177 5.9681ZM12 20C15.866 20 19 16.866 19 13C19 9.13401 15.866 6 12 6C8.13401 6 5 9.13401 5 13C5 16.866 8.13401 20 12 20ZM11 8H13V14H11V8ZM8 1H16V3H8V1Z",
      }),
    );
  }
}


export const Connectors = {

  block: null,
  block_width: 400, // Define statically (in px)
  block_height: 200,

  oninit({ attrs }) {
    const { block, width = 400, height = 200, ...props } = attrs;

    this.block = block;
    this.block_width = width;
    this.block_height = height;

  },

  view({ attrs }) {
    const { z_index = "z-20", onConnectorClick = null, ...props } = attrs;

    let output = {
      x: this.block.position.x + this.block_width,
      y: this.block.position.y + (this.block_height / 2),
    };

    let inputs = [];
    let num_inputs = this.block.num_inputs + 1;
    this.block.inputs.forEach((input, index) => {
      let position = {
        x: this.block.position.x,
        y: this.block.position.y + Math.floor(this.block_height * (index + 1) / num_inputs),
      }
      inputs.push(position);
    });

    return m("svg", { class: "absolute top-0 w-dvw h-dvh pointer-events-none " + z_index },
      // Inputs (left side)
      inputs.map((input, i) => [
        m('path', {
          id: `${this.block.id}-input${i}`,
          data_element_type: "Connector",
          class: "fill-none stroke-[20px] stroke-black dark:stroke-white pointer-events-auto",
          d: `M${input.x},${input.y},${input.x + 5},${input.y}`,
          onmousedown: (event) => { onConnectorClick(event, this.block, i+1, input) },
        }),
      ]),
      // Output (right side)
      this.block.num_outputs &&
      m('path', {
        id: `${this.block.id}-output`,
        data_element_type: "Connector",
        class: "fill-none stroke-[20px] stroke-black dark:stroke-white pointer-events-auto",
        d: `M${output.x - 5},${output.y},${output.x},${output.y}`,
        onmousedown: (event) => { onConnectorClick(event, this.block, 0, output) },
      }),
    );
  }
}
