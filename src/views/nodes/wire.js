import m from "mithril";


export const Connection = {

  from: { x: 100, y: 100 },
  to: { x: 200, y: 200 },

  id: null,
  start_block: null,
  end_block: null,
  block_width: 400, // Define statically (in px)
  block_height: 200,

  oninit({ attrs }) {
    const { from, to, width = 400, height = 200, ...props } = attrs;

    this.start_block = from;
    this.end_block = to;
    this.block_width = width;
    this.block_height = height;
    this.id = this.start_block.id + "-" + this.end_block.id;
  },

  view({ attrs }) {

    if (this.start_block) {
      this.from = { // Middle of right edge
        x: this.start_block.position.x + this.block_width,
        y: this.start_block.position.y + (this.block_height / 2),
      };
    }
    if (this.end_block) {
      let num_inputs = this.end_block.num_inputs + 1;
      let input_index = this.end_block.inputs.indexOf(this.start_block) + 1;

      this.to = { // left edge based on index
        x: this.end_block.position.x,
        y: this.end_block.position.y + Math.floor(this.block_height * input_index / num_inputs),
      };
    }

    return [
      m(Wire, { id: this.id, from: this.from, to: this.to }),
    ];
  }
}


// Heavily inspired by: https://github.com/emilwidlund/wire/blob/master/artifacts/wire-ui/src/components/Connection/Connection.tsx
export const Wire = {

  id: null,
  from: { x: 100, y: 100 },
  to: { x: 200, y: 200 },

  oninit({ attrs }) {
    const { id, ...props } = attrs;

    this.id = id;
  },

  view({ attrs }) {
    const { from, to, z_index = "z-10", ...props } = attrs;

    this.from = from;
    this.to = to;

    let path = bezierCurve(this.from, this.to);

    return m("wire",
      m("svg", { class: "absolute top-0 w-dvw h-dvh pointer-events-none " + z_index },
        // wire
        m('path', {
          id: this.id,
          data_element_type: "Wire",
          class: "fill-none stroke-[6px] stroke-orange-500 pointer-events-auto",
          d: path,
        }),
      )
    );
  }

}


const bezierCurve = (start, end) => {
    let x1 = start.x;
    let y1 = start.y;
    let x4 = end.x;
    let y4 = end.y;
    let min_diff = 42;
    let diffx = Math.max(min_diff, x4 - x1);
    let diffy = Math.max(min_diff, y4 - y1);

    let x2 = x1 + diffx * 0.5;
    let y2 = y1;
    let x3 = x4 - diffx * 0.5;
    let y3 = y4;

    return `M${x1.toFixed(3)},${y1.toFixed(3)} C${x2},${y2} ${x3},${y3}  ${x4.toFixed(3)},${y4.toFixed(3)}`;
};
