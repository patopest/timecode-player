import m from "mithril";
import { Dropdown, ListGroup } from "flowbite-mithril";
import { CloseIcon, TrashBinIcon } from "flowbite-icons-mithril/solid";

import { Blocks } from "../../blocks";
import { Node } from "./node";
import { Connection, Wire } from "./wire";



const ContextMenu = {

  pos: { x: 0, y: 0 },
  type: "",
  block: null,
  to_block: null,

  oninit({ attrs }) {
    const { pos, type, from_block, to_block, ...props } = attrs;

    if (pos) {
      this.pos = pos;
    }
    this.type = type;
    this.block = from_block;
    this.to_block = to_block;
  },

  view({ attrs }) {

    const onClick = (event) => {

      if (this.type === "Node") {
        this.block.delete();
      }
      else if (this.type == "Wire") {
        this.to_block.disconnect_input(this.block);
      }
    }

    let transform = "transform: translate3d(" + this.pos.x + "px, " + this.pos.y + "px, 0)";

    return m("div", { class: "absolute top-0 flex justify-center" }, [
      m(ListGroup, { class: "w-48 z-50", style: transform },
        this.type === "Wire" && [
          m(ListGroup.Item, { onclick: onClick, icon: CloseIcon }, "Disconnect"),
        ],
        this.type === "Node" && [
          m(ListGroup.Item, { onclick: onClick, icon: TrashBinIcon }, "Delete"),
        ]
        ),
    ]);
  }
}


export const NodesPanel = {

  mouse_pos: { x: 0, y: 0 },

  new_wire: false,
  new_wire_start: {
    pos: { x: 0, y: 0 },
    block: null,
    connector: null,
  },

  context_menu: false,
  context_menu_props: {
    type: "",
    from_block: null,
    to_block: null
  },

  view({ attrs }) {

    const onConnectorClick = (event, block, connector, connector_pos) => {
      this.new_wire = true;
      this.mouse_pos.x = event.layerX;
      this.mouse_pos.y = event.layerY;
      this.new_wire_start.pos = connector_pos;
      this.new_wire_start.block = block;
      this.new_wire_start.connector = connector;
    }

    const onDragEnd = (event) => {

      let elements = document.elementsFromPoint(event.clientX, event.clientY);
      for (const element of elements) {
        const type = element.getAttribute("data_element_type");

        if (type === "Connector") {
          const [block_dest, connector] = element.id.split("-");
          let block = Blocks.find((block) => block.id === block_dest);

          if (connector === "output" && this.new_wire_start.connector > 0 && block) {
            this.new_wire_start.block.connect_input(this.new_wire_start.connector - 1, block);
          }
          else { // connector === "inputX"
            let input_idx = connector.substring(5);
            if (this.new_wire_start.connector == 0 && block) { // wire going from output to input
              block.connect_input(input_idx, this.new_wire_start.block);
            }
          }
        }
      } 

      this.new_wire = false;
    }

    const onDrag = (event) => {
      if (this.new_wire) {
        event.preventDefault();
        this.mouse_pos.x = event.layerX;
        this.mouse_pos.y = event.layerY;
      }
    }

    const onContextClick = (event) => {
      event.preventDefault();
      this.context_menu = false;
      this.mouse_pos.x = event.layerX;
      this.mouse_pos.y = event.layerY;

      let elements = document.elementsFromPoint(event.clientX, event.clientY);
      for (const element of elements) {
        const type = element.getAttribute("data_element_type");

        if (type === "Wire") {
          this.context_menu_props.type = "Wire";
          const [block_source, block_dest] = element.id.split("-");
          let from_block = Blocks.find((block) => block.id === block_source);
          let to_block = Blocks.find((block) => block.id === block_dest);
          this.context_menu_props.from_block = from_block;
          this.context_menu_props.to_block = to_block;
          this.context_menu = true;
        }
        else if (type === "Node") {
          this.context_menu_props.type = "Node";
          let block = Blocks.find((block) => block.id === element.id);
          this.context_menu_props.from_block = block;
          this.context_menu = true;
        }
      }
    }

    const onClick = (event) => {
      this.context_menu = false;
    }

    return m("div", {
        class: "relative top-0 w-screen h-screen",
        onmouseup: onDragEnd,
        onmousemove: onDrag,
        oncontextmenu: onContextClick,
        onclick: onClick,
      },
      Blocks.map((block) => {
        return [
          // Node
          [
            m(Node, { key: block.id, block: block, onConnectorClick: onConnectorClick })
          ],
          // Connections
          block.outputs.map((output) => [
            m(Connection, { key: `${block.id}-${output.id}`, from: block, to: output })
          ])
        ];
      }),
      this.new_wire && m(Wire, { from: this.new_wire_start.pos, to: this.mouse_pos, z_index: "z-50" }),
      this.context_menu && m(ContextMenu, { pos: this.mouse_pos, ...this.context_menu_props }),
    );
  }
}