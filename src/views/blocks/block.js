import m from "mithril";
import { Button, Dropdown, Card } from "flowbite-mithril";
import { TrashBinIcon } from "flowbite-icons-mithril/solid";

import { Clock } from "../clock";
import { Blocks } from "../../blocks";
import { Settings } from "../../models/settings";


const BlockPanel = {
  block: null,

  oninit({ attrs }) {
    const { block, ...props } = attrs;

    this.block = block;
    console.log("oninit Block Panel", this.block.id);
  },

  oncreate({ attrs }) {
    this.block.on_change(() => {
      m.redraw();
    })
  },

  view({ attrs, children }) {
    const { block, ...props } = attrs;

    const deleteNode = () => {
      this.block.delete();
    }

    return m(Card, { style: "container-type: inline-size;", ...props },
      m("div", { class: "flex gap-3 px-2 justify-between text-center items-center"}, 
        m("h1", { class: "text-xl" }, this.block.name),
        m("div", { class: "flex" },
          // children are only the specific controls in the top bar of the car
          children,
          m("button", { class: "ml-8", onclick: deleteNode }, m(TrashBinIcon)),
        )
      ),
      m(Clock, {time: this.block.timecode }),
    )
  }

}


export const ReceiverPanel = {
  block: null,

  oninit({ attrs }) {
    const { block, ...props } = attrs;

    this.block = block;
  },

  view({ attrs, children }) {

    return m(BlockPanel, { block: this.block },
      // Input device dropdown
      m(Dropdown, { id: `${this.block.id}-device`, label: this.block.device, ignoreClickOutsideClass: true, trigger: "hover" },
        [
          this.block.list_devices().map(device => [
            m(Dropdown.Item, { onclick: () => { this.block.select_device(device) } }, device),
          ]),
        ],
      ),
    )
  }
}


export const OperatorPanel = {
  block: null,

  oninit({ attrs }) {
    const { block, ...props } = attrs;

    this.block = block;
  },

  view({ attrs, children }) {

    return m(BlockPanel, { block: this.block },
      m("div", { class: "flex justify-between gap-3" },
        // Dropdown for each input connection
        this.block.inputs.map((input, i) => {
          let selected_input = this.block.inputs[i] ? this.block.inputs[i].name : "";

          return m(Dropdown, { id: `${this.block.id}-input${i}`, label: selected_input, ignoreClickOutsideClass: true, trigger: "hover" },
            [
              // Dropdown item for each available other block
              Blocks.reduce((items, node) => {
                if (node != this.block) {
                  items.push(
                    m(Dropdown.Item, { onclick: () => { this.block.connect_input(i, node) } }, node.name)
                  );
                }
                return items;
              }, []),
            ],
          );
        }),
      ),
    )
  }
}


export const SenderPanel = {
  block: null,

  oninit({ attrs }) {
    const { block, ...props } = attrs;

    this.block = block;
  },

  view({ attrs, children }) {

    let selected_input = this.block.inputs[0] ? this.block.inputs[0].name : "";

    return m(BlockPanel, { block: this.block },
      m("div", { class: "flex justify-between gap-3" },
        // Input connection dropdown
        m(Dropdown, { id: `${this.block.id}-input`, label: selected_input, ignoreClickOutsideClass: true, trigger: "hover" },
          [
            // Dropdown item for each available other block
            Blocks.reduce((items, node) => {
              if (node != this.block) {
                items.push(
                  m(Dropdown.Item, { onclick: () => { this.block.connect_input(0, node) } }, node.name)
                );
              }
              return items;
            }, []),
          ],
        ),
        // Output device dropdown
        m(Dropdown, { id: `${this.block.id}-device`, label: this.block.device, ignoreClickOutsideClass: true, trigger: "hover" },
          [
            this.block.list_devices().map(device => [
              m(Dropdown.Item, { onclick: () => { this.block.select_device(device) } }, device),
            ]),
          ],
        ),
      ),
    )
  }
}

