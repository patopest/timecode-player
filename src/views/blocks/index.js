export * from "./block";


import { ReceiverPanel, OperatorPanel, SenderPanel } from "./block";
import {
    Block,
    MTCReceiver, LTCReceiver,
    SumOperator, DiffOperator, OffsetOperator,
    MTCSender, LTCSender
} from "../../blocks";

export const PanelTypes = {
    "MTCReceiver":      ReceiverPanel,
    "LTCReceiver":      ReceiverPanel,
    "SumOperator":      OperatorPanel,
    "DiffOperator":     OperatorPanel,
    "OffsetOperator":   OperatorPanel,
    "MTCSender":        SenderPanel,
    "LTCSender":        SenderPanel,
}