import m from "mithril";

import { Settings } from "../models/settings";



const NumberBlock = {
  view({ attrs }) {
    const { num, name, subname, ...props } = attrs;

    // Reprensent number with 2 digits
    var num_string = "".concat(
      num < 10 ? '0' : '',
      num.toString(),
    );

    let theme = "grid grid-rows-2";
    if (Settings.show_labels) {
      theme = "grid grid-rows-3";
    }
    theme += " text-center";

    let label_size = "text-[4cqw]";

    return m("div", { class: theme },
      m("number", { class: `font-mono row-span-2` }, num_string),
      // m("div", { class: "grid grid-rows-2" },
      Settings.show_labels ? 
        m("text", { class : "text-zinc-500 justify-self-center " + label_size }, name)
      : null
        // subname ? m("text", { class : "text-lg text-zinc-500 justify-self-end" }, subname) : null,
      // ),
      // subname ? m("text", { class : "text-lg text-zinc-500 justify-self-end" }, subname) : null,
    )
  }
}


export const Clock =  {

  view({ attrs }) {
    const { time, ...props } = attrs;

    let text_size = "text-[12cqw]";

    return m("container", { class: "flex flex-initial h-full justify-center text-center items-center gap-px " + text_size,
         },
      [
        m(NumberBlock, { name: "hours", num: time.hours }),
        m("text", { class: "place-self-start" }, ":"),
        m(NumberBlock, { name: "minutes", num: time.minutes }),
        m("text", { class: "place-self-start" }, ":"),
        m(NumberBlock, { name: "seconds", num: time.seconds }),
        m("text", { class: "place-self-start" }, time.dropFrame ? ";" : ":"),
        m(NumberBlock, { name: "frames", num: time.frames }),
      ]
    )
  }
}


