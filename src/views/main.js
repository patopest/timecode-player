import m from "mithril";
import { Button, Dropdown } from "flowbite-mithril";
import { BarsIcon, GridIcon, ColumnIcon, ShareNodesIcon, CirclePlusIcon, } from "flowbite-icons-mithril/solid";

import {
  Blocks,
  MTCReceiver, LTCReceiver,
  SumOperator, DiffOperator, OffsetOperator,
  MTCSender, LTCSender
} from "../blocks";
import { PanelTypes } from "./blocks";
import { NodesPanel } from "./nodes";
import { Settings } from "../models/settings";

export const BlocksControlBar = {
  view() {
    let display_mode = Settings.display_mode;

    const set_display_mode = (mode) => {
      Settings.set_display_mode(mode);
    }

    return m("div", { class: "z-50 flex justify-between my-3 mx-5" },
      m(Dropdown, { id: "blocks-add-dropdown", class: "z-50", color: "gray", label: m(CirclePlusIcon), arrowIcon: false },
        m(Dropdown.Item, "Generator"),
        m(Dropdown.Divider),
        m(Dropdown.Item, { onclick: () => { new MTCReceiver() } }, "MTC Receiver"),
        m(Dropdown.Item, { onclick: () => { new LTCReceiver() } }, "LTC Receiver"),
        m(Dropdown.Divider),
        m(Dropdown.Item, { onclick: () => { new SumOperator() } }, "Sum Operator"),
        m(Dropdown.Item, { onclick: () => { new DiffOperator() } }, "Sub Operator"),
        m(Dropdown.Item, { onclick: () => { new OffsetOperator() } }, "Offset Operator"),
        m(Dropdown.Divider),
        m(Dropdown.Item, { onclick: () => { new MTCSender() } }, "MTC Sender"),
        m(Dropdown.Item, { onclick: () => { new LTCSender() } }, "LTC Sender"),
      ),
      m(Button.Group, [
        m(Button, { color: display_mode == "nodes"  ? "dark" : "gray", onclick: () => { set_display_mode("nodes")  } }, m(ShareNodesIcon, { class: "" }), null),
        m(Button, { color: display_mode == "single" ? "dark" : "gray", onclick: () => { set_display_mode("single") } }, m(BarsIcon,       { class: "" }), null),
        m(Button, { color: display_mode == "double" ? "dark" : "gray", onclick: () => { set_display_mode("double") } }, m(GridIcon,       { class: "" }), null),
        m(Button, { color: display_mode == "triple" ? "dark" : "gray", onclick: () => { set_display_mode("triple") } }, m(ColumnIcon,     { class: "" }), null),
      ]),
    );
  }
}


export const BlocksPanel = {
  view({ attrs }) {
    let display_mode = Settings.display_mode;

    let theme = ""
    if (display_mode === "single") {
      theme += "grid m-3 gap-3"
    }
    else if (display_mode === "double") {
      theme += "grid grid-cols-2 m-3 gap-3"
    }
    else if (display_mode === "triple") {
      theme += "grid grid-cols-3 m-3 gap-3"
    }

    if (display_mode === "nodes") {
      return m(NodesPanel);
    }
    else {
      return m("div", { class: theme },
        Blocks.map((block) => {
          let component = PanelTypes[block.type];

          return m(component, { block: block, key: block.id });
        })
      );
    }
  }
}

