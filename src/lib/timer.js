import Timecode from "smpte-timecode";


// https://stackoverflow.com/questions/29971898/how-to-create-an-accurate-timer-in-javascript
// TODO: it's okay for now but drifts... need a better way
export function Timer(workFunc, interval, errorFunc) {
    var that = this;
    var expected, timeout;
    this.interval = interval;

    this.start = function() {
        expected = Date.now() + this.interval;
        timeout = setTimeout(step, this.interval);
    }

    this.stop = function() {
        clearTimeout(timeout);
    }

    function step() {
        var drift = Date.now() - expected;
        if (drift > that.interval) {
            // You could have some default stuff here too...
            if (errorFunc) errorFunc();
        }
        workFunc();
        expected += that.interval;
        timeout = setTimeout(step, Math.max(0, that.interval-drift));
    }
}



export function TimecodeClock(mode, start_now) {
    console.log("Init clock with mode: " + mode);

    var that = this;
    this.mode = mode;
    this.running = start_now;
    this.framerate = 30;
    this.time = Timecode('00:00:00:00', this.framerate);
    this.callback = null;
    this.timer = null;


    // function init_timer() {
    this.init_timer = function() {
        var interval = Math.round(1000 / this.framerate);
        console.log("init clock timer with interval ", interval, this.framerate);
        this.timer = new Timer(tick.bind(this), interval, null);
        if (this.running) {
            this.start();
        }
    }

    this.start = function() {
        console.log("Starting clock");
        if (this.timer == null) {
            this.init_timer();
        }
        this.running = true;
        this.timer.start();
    }


    this.stop = function() {
        console.log("Stopping clock");
        this.running = false;
        this.timer.stop();
    }


    this.toggle = function() {
        if (this.running) {
            this.stop();
        }
        else {
            this.start();
        }

    }

    this.set_time = function(time) {
        try {
            this.time = Timecode(time);
            if (this.time.frameRate != this.framerate) { // reinit timer if framerate changed
                this.framerate = this.time.frameRate;
                console.log("reinit timer");
                this.init_timer();
            }
            if (this.callback) this.callback(time); // TBD if this should be done here
        }
        catch (err) {
            console.error(err);
        }
    }

    this.on_change = function(callback) {
        this.callback = callback;
    }

    function tick() {
        if (this.running) {
            if (this.mode === "generator") {
                this.time.add(1);
                if (this.callback) {
                    this.callback(this.time);
                }
            }
            else if (this.mode === "monitor") {
                // TBD if we poll here or only use a callback on set_time()
            }
        }
    }
}
