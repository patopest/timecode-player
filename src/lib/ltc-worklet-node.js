import { Decoder, Encoder, Frame } from "linear-timecode";


class LTCDecoderProcessor extends AudioWorkletProcessor {

  constructor(options) {
    super();
    
    this.decoder = new Decoder(44100);
    this.decoder.on("frame", (timecode) => {
      // console.log(JSON.stringify(timecode));
      this.port.postMessage(JSON.stringify(timecode)); // serialise object to string to pass as a message
    });

  }

  process(inputs, outputs, parameters) {
    
    if (inputs.length > 0) {
      const input = inputs[0];
      if (input.length > 0) {
        try {
          this.decoder.decode(input[0]);
        }
        catch (err) {
          console.error("Unable to decode timecode:", err);
        }
        return true;
      }
    }

    return false;
  }
}

registerProcessor("ltc-decoder-processor", LTCDecoderProcessor);



class LTCEncoderProcessor extends AudioWorkletProcessor {

  constructor(options) {
    super();
    
    this.encoder = new Encoder(44100);
    this.offset = 0;
    this.buffer = [];
    this.new_buffer = [];
    this.timecode = null;
    this.new_timecode = null;
    this.new_timecode_available = false;
    this.port.onmessage = (event) => {
      // Handling data from the main thread
      this.timecode = event.data;

      let frame = new Frame(this.timecode.frameRate);
      frame.hours = this.timecode.hours;
      frame.minutes = this.timecode.minutes;
      frame.seconds = this.timecode.seconds;
      frame.frames = this.timecode.frames;
      frame.dropframe = this.timecode.dropFrame; 

      this.new_buffer = this.encoder.encode(frame);
      this.new_timecode_available = true;

      // special case of first start, need to compute buffer
      if (this.buffer.length == 0) {
        this.buffer = this.encoder.encode(frame);
      }
    };
  }

  process(inputs, outputs, parameters) {
    
    if (outputs.length > 0) {
      const output = outputs[0];
      const channel = output[0];
      
      if (this.offset + channel.length > this.buffer.length) {
        let split = this.buffer.length - this.offset;

        // console.log("split", this.offset, split, channel.length - split)

        // fill rest of buffer with current samples
        for (let i = 0; i < split; i++) {
          channel[i] = this.buffer[this.offset + i] / 127;
        }

        if (this.new_timecode_available) {
          this.buffer = this.new_buffer;
          this.new_timecode_available = false;
        }

        // fill rest of buffer with new samples
        for (let i = split, j = 0; i < channel.length; i++, j++) {
          channel[i] = this.buffer[j] / 127;
        }
        this.offset = channel.length - split;
      }
      else {
        // console.log("fill", this.offset, this.offset+channel.length);
        // fill output with samples
        for (let i = 0; i < channel.length; i++) {
          channel[i] = this.buffer[this.offset + i] / 127;
        }
        this.offset += channel.length;
      }
      
      return true;
    }

    return false;
  }

}

registerProcessor("ltc-encoder-processor", LTCEncoderProcessor);

