
const filter = { usbVendorId: 0xCafe }; // to be used with final product
var devices = []
var selected_device = null;
var enabled = false;
var supported = 'serial' in navigator;


function enable() {

    if (!supported) {
        console.warn("WebSerial is not supported");
        return;
    }

    navigator.serial.getPorts()
        .then((ports) => {
          console.log("WebSerial enabled!");
          enabled = true;
          console.log("Serial devices", ports);
          devices = ports;
          if (devices.length > 0) { // autoselect if already device already paired
            selected_device = devices[0];
          }
        })
        .catch(err => console.error(err));
}


// Due to the webserial API, this function need to be called on a user action (eg: button click)
function select_device() {

    if (!supported) {
        console.warn("WebSerial is not supported");
        return;
    }    

    // navigator.serial.requestPort({ filters: [filter] }) # When we'll have proper vid and pid in the nixie clock
    navigator.serial.requestPort()
        .then((device) => {
          console.log("Succesfully connected to serial device", device);
          selected_device = device;
        })
        .catch(err => console.error(err));
}


function get_device_info() {

    if (selected_device) {
        const info = selected_device.getInfo();
        return true;
    }

    return false;
}




export const Serial = {
    init: enable,
    // disable: disable,
    enabled: function() { return enabled; },
    supported: function() { return supported; },
    // list_devices: list_devices,
    // get_available_devices: function() { return devices; },
    select_device: select_device,
    // get_device: function() { return selected_device; },
    get_device: get_device_info,
    // start_read: start_read,
    // get_timecode: function() { return received_timecode; },
    // on_timecode: function(callback) { timecode_callback = callback; },
}