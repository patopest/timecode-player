import Timecode from "smpte-timecode";
import audioWorkletUrl from "./ltc-worklet-node.js?worker&url";

var supported = 'mediaDevices' in navigator;
var input_devices = [];
var output_devices = [];

let audio_ctx = null;
let audio_input_streams = {};
let audio_output_streams = {};

function enable() {
  if (!supported) {
    console.warn("WebAudio is not supported");
    return;
  }
    
  audio_ctx = new AudioContext({sampleRate: 44100, latencyHint: "interactive"});
  audio_ctx.audioWorklet.addModule(audioWorkletUrl)
    .then(() => {
      console.log("Audio worklet module loaded")
    })
    .catch((err) => {
      console.error("Unable to load audio worklet module:",err);
    });
  console.log("Audio enabled");

  // This triggers to browser to ask for permissions to access user media
  navigator.mediaDevices.getUserMedia({ audio: true })
    .then(() => {
      list_devices();
    })
}


function disable() {
  if (audio_ctx) {
    audio_ctx.close().then(() => {
      console.log("Suspended audio context");
      audio_ctx = null;
      input_devices = [];
      output_devices = [];
    });
  }
}


function list_devices() {

  if (!supported) {
    console.warn("WebAudio is not supported");
    return;
  } 

  navigator.mediaDevices.enumerateDevices()
    .then((devices) => {
      input_devices = devices.filter((device) => device.kind === 'audioinput');
      console.log('Audio input devices:', input_devices);
      output_devices = devices.filter((device) => device.kind === 'audiooutput');
      console.log('Audio output devices:', output_devices);
    })
    .catch((err) => {
      console.log(`${err.name}: ${err.message}`);
  });
}


function get_input_devices() {
  const devices = input_devices.map((device) => device.label);
  return devices;
}


function get_output_devices() {
  const devices = output_devices.map((device) => device.label);
  return devices;
}


function get_devices() {

  var devices = {};
  input_devices.forEach((device) => {
    devices[device.label] = {
      "input": true,
      "output": false,
    };
  });
  output_devices.forEach((device) => {
    if (device.label in devices) {
      devices[device.label].output = true;
    }
    else {
      devices[device.label] = {
        "input": false,
        "output": true,
      };
    }
  });

  return devices;
}


function unselect_input_device(name) {

  const device = input_devices.find(d => d.label.includes(name))
  if (device) {
    const stream = audio_input_streams[name];
    if (stream) {
      stream.source.disconnect();
      // delete stream.source;
      // delete stream.ltc_node;
      delete audio_input_streams[name];
    }
  }
}

function unselect_output_device(name) {

  const device = output_devices.find(d => d.label.includes(name))
  if (device) {
    const stream = audio_output_streams[name];
    if (stream) {
      stream.dest.disconnect();
      stream.ltc_node.disconnect();
      stream.element.srcObject = null;
      // delete stream.dest;
      // delete stream.ltc_node;
      delete audio_output_streams[name];
    }
  }
}


function read(name, callback) {

  const device = input_devices.find(d => d.label.includes(name))

  if (audio_ctx == null || device == null) {
    console.warn("Unable to start reading device");
    return;
  }

  let device_constraints = {
    audio: {
      autoGainControl: false,
      echoCancellation: false,
      noiseSuppression: false,
      deviceId: device.deviceId
    }
  };

  navigator.mediaDevices
    .getUserMedia(device_constraints)
    .then((stream) => {
        const source = audio_ctx.createMediaStreamSource(stream);
    
        const ltc_decoder_node = new AudioWorkletNode(
          audio_ctx,
          "ltc-decoder-processor",
          { numberOfInputs: 1, numberOfOutputs: 0 },
        );
        ltc_decoder_node.onprocessorerror = (event) => {
            console.error(event);
        }
        ltc_decoder_node.port.onmessage = (e) => {
          try {
            const tc_obj = JSON.parse(e.data);
            // console.log(tc_obj)
            let timecode = new Timecode(tc_obj, tc_obj.framerate, tc_obj.dropframe);
            if (callback) {
              callback(0, timecode);
            }
          }
          catch (err) {
            console.error("Unable to decode", err);
          }
        }
        
        // route audio from source to ltc decoder node
        source.connect(ltc_decoder_node);
        audio_input_streams[name] = {
          source: source,
          ltc_node: ltc_decoder_node,
        }

        console.log("Audio, starting to read: ", name);

        // audio_ctx.onstatechange = function () {
        //     console.log(audio_ctx.state);
        // };

    })
    .catch((err) => {
        console.log("The following getUserMedia error occurred:", err);
    });
}


function setup_output(name) {

  const device = output_devices.find(d => d.label.includes(name))

  if (audio_ctx == null || device == null) {
    console.warn("Unable to start reading device");
    return;
  }

  let device_constraints = {
    audio: {
      autoGainControl: false,
      echoCancellation: false,
      noiseSuppression: false,
      deviceId: device.deviceId
    }
  };

  navigator.mediaDevices
    .getUserMedia(device_constraints)
    .then((stream) => {
        const dest = audio_ctx.createMediaStreamDestination(stream);
    
        const ltc_encoder_node = new AudioWorkletNode(
          audio_ctx,
          "ltc-encoder-processor",
          { numberOfInputs: 0, numberOfOutputs: 1, outputChannelCount: [3] },
        );
        ltc_encoder_node.onprocessorerror = (event) => {
            console.error(event);
        }
        ltc_encoder_node.port.postMessage(
          new Timecode("01:00:00:00")
        );
        
        // route audio from ltc encoder node to destination
        ltc_encoder_node.connect(dest);

        // Create new HTML to route audio to custom destination
        var audio = new Audio();
        audio.setSinkId(device.deviceId)
          .then(() => {
            audio.srcObject = dest.stream;
            audio.play();

            console.log("Audio, starting to send: ", name);
          })

        audio_output_streams[name] = {
          dest: dest,
          ltc_node: ltc_encoder_node,
          element: audio,
        }

    })
    .catch((err) => {
        console.log("The following getUserMedia error occurred:", err);
    });
}


function send(name, timecode) {
  const stream = audio_output_streams[name];
  if (stream) {
    stream.ltc_node.port.postMessage(
      timecode
    );
  }
}



export const LTC = {
    init: enable,
    disable: disable,
    enabled: function() { return audio_ctx; },
    supported: function() { return supported; },
    list_devices: list_devices,
    get_input_devices: get_input_devices,
    get_output_devices: get_output_devices,
    get_devices: get_devices,
    unselect_input_device: unselect_input_device,
    unselect_output_device: unselect_output_device,
    read: read,
    setup_output: setup_output,
    send: send,
}