import { WebMidi } from "webmidi";
import Timecode from "smpte-timecode";

var supported = WebMidi.supported;
var input_devices = [];
var output_devices = [];


function enable() {
  if (!supported) {
    console.warn("WebMidi is not supported");
    return;
  }

  WebMidi
    .enable({sysex: true})
    .then(() => {
      console.log("WebMidi with sysex enabled!");
      list_devices();
    })
    .catch(err => console.error(err));
}


function disable() {
  WebMidi
    .disable()
    .then(() => {
      console.log("WebMidi is disabled!");
      input_devices = [];
      output_devices = []
    })
    .catch(err => console.error(err));
}


function list_devices() {
  if (!supported) {
    console.warn("WebMidi is not supported");
    return;
  }

  // Inputs
  var inputs = WebMidi.inputs.map((input) => input.name );
  console.log("Midi input devices", inputs);
  
  // Outputs
  var outputs = WebMidi.outputs.map((input) => input.name );
  console.log("Midi output devices", outputs);

  input_devices = inputs;
  output_devices = outputs;
}


function get_devices() {

  var devices = {};
  input_devices.forEach((device) => {
    devices[device] = {
      "input": true,
      "output": false,
    };
  });
  output_devices.forEach((device) => {
    if (device in devices) {
      devices[device].output = true;
    }
    else {
      devices[device] = {
        "input": false,
        "output": true,
      };
    }
  });

  return devices;
}


function select_device(name) {
  if (WebMidi.enabled == false || device == null) {
    console.warn("Unable to select device");
    return;
  }
  // TODO: Avoid this function being executed before init finishes.
  
  // Remove existing listeners defined in start function
  if (selected_device) {
    var input = WebMidi.getInputByName(name);
    input.removeListener();
    // received_timecode = new Timecode('00:00:00:00');
  }
    
  console.log("Selecting Midi device:", name);
  // selected_device = name;
}


function unselect_device(name) {
  if (WebMidi.enabled == false || name == null) {
    console.warn("Unable to unselect device");
    return;
  }
  
  // Remove existing listeners defined in start function
  var input = WebMidi.getInputByName(name);
  if (input) {
    input.removeListener();
  }
    
  console.log("Unselecting Midi device:", name);
}


function read(device, callback) {
  if (WebMidi.enabled == false || device == null) {
    console.warn("Unable to start reading device");
    return;
  }
  
  var input = WebMidi.getInputByName(device);
  console.log("Starting to read", input.name);
  input.addListener("timecode", e => {
    console.debug(e.timecode);
  })
  input.addListener("sysex", e => {
    decode_sysex(e.message.dataBytes, callback);
  })
}


function decode_sysex(data, callback) {

  if(data[0] != 0x7F) { // <device ID> = 0x7F intended for full system
    return;
  }
  if (data[1] == 0x01 && data[2] == 0x01) { // <sub ID 1> = 0x01 (Midi Timecode), <sub ID 2> = 0x01 (Full Timecode Message) 
    var type = data[3] >> 5;
    var time = {
      hours: data[3] & 0x1F,
      minutes: data[4],
      seconds: data[5],
      frames: data[6],
    }

    var dropframe = false;
    var framerate = 0;
    if (type == 0b00) framerate = 24;
    else if (type == 0b01) framerate = 25;
    else if (type == 0b10) {framerate = 30; dropframe = true;}
    else framerate = 30;

    try {
      var timecode = new Timecode(time, framerate, dropframe);
      // console.debug("Midi TC:", timecode.toString(), timecode.frameRate, timecode.dropFrame);
      if (callback) {
        callback(0, timecode);
      }
    }
    catch (err){
      console.error(time, framerate, dropframe, err);
    }
  }
}


function send(device, timecode) {
  if (WebMidi.enabled == false || device == null) {
    // console.warn("Unable to send to device");
    return;
  }

  let data = encode_sysex(timecode);

  var output = WebMidi.getOutputByName(device);
  output.sendSysex(0x7F, data); // 0x7F: Universal realtime message


}

function encode_sysex(timecode) {

  const data = new Uint8Array(7);
  data[0] = 0x7F; // <device ID> = 0x7F intended for full system
  data[1] = 0x01; // <sub ID 1> = 0x01 (Midi Timecode)
  data[2] = 0x01; // <sub ID 2> = 0x01 (Full Timecode Message)

  data[4] = timecode.minutes;
  data[5] = timecode.seconds;
  data[6] = timecode.frames;

  let framerate = timecode.frameRate;
  let dropframe = timecode.dropFrame;
  let type = 0b00;
  if (framerate == 24) type = 0b00;
  else if (framerate == 25) type = 0b01;
  else if (framerate == 30 && dropframe) type = 0b10;
  else type = 0b11; // 30 fps but not dropframe

  data[3] = (type << 5) | (timecode.hours & 0x1F);

  return data;
}



export const MTC = {
    init: enable,
    disable: disable,
    enabled: function() { return WebMidi.enabled; },
    supported: function() { return supported; },
    list_devices: list_devices,
    get_input_devices: function() { return input_devices; },
    get_output_devices: function() { return output_devices; },
    get_devices: get_devices,
    select_device: select_device,
    unselect_device: unselect_device,
    read: read,
    send: send,
}