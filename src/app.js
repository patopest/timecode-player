import m from "mithril";
import { Navbar, Footer, Button } from "flowbite-mithril";
import { MoonIcon, SunIcon, GithubIcon } from "flowbite-icons-mithril/solid";
import { CogIcon } from "flowbite-icons-mithril/outline";

import "./app.css";
import { BlocksPanel, BlocksControlBar } from "./views/main";
import { SettingsPanels } from "./views/settings";
import { MTC } from "./lib/mtc";
import { LTC } from "./lib/ltc";
import { Serial } from "./lib/serial";
import { localStore } from "./lib/storage";
import { Timer } from "./lib/timer";
import { Settings } from "./models/settings";
import { Blocks, ImportBlocks } from "./blocks";

const theme = localStore('theme');


let timer = null;
const runBlocks = () => {
  for (const block of Blocks) {
    block.update();
  }
}

const setTimer = () => {
  var interval = Math.round(1000 / Settings.default_framerate);
  timer = new Timer(runBlocks, interval, null);
  timer.start();
}


function DarkModeButton(initialVnode) {

  function toggleDarkMode() {
    if (theme.get() === 'dark') {
      theme.set('light');
      document.documentElement.classList.remove('dark')
    } else {
      theme.set('dark');
      document.documentElement.classList.add('dark')
    }
  }

  return {
    view: function(vnode) {
      if (theme.get() === 'dark') {
        return m(Button, { onclick: toggleDarkMode, ...vnode.attrs },
          m(MoonIcon),
        )
      }
      else {
        return m(Button, { onclick: toggleDarkMode, ...vnode.attrs },
          m(SunIcon),
        )
      }
    }
  }
}


const Top = {
  view: function(vnode) {

    return m("div",
      m(Navbar, { class: "list-none bg-gray-100 text-zinc-900 dark:bg-zinc-900 dark:text-zinc-300"},
        [
          m(Navbar.Brand, { href: "/", class: "text-xl" }, "Timecode Player"),

          m("div", { class: "flex justify-center items-center space-x-10"},
            m(Navbar.Link, { href: "/" }, "Blocks"),
            // m(Navbar.Link, { href: "/player" }, "Player"),
            // m(Navbar.Link, { href: "/manager" }, "Manager"),
          ),

          m("div", { class: "flex justify-center items-center"},
            m("a", { href: "https://gitlab.com/patopest/timecode-player" },
              m(Button, { class: "text-zinc-900 bg-transparent enabled:hover:bg-gray-200 dark:bg-transparent dark:text-zinc-300 enabled:hover:dark:bg-zinc-800 focus:ring-transparent dark:focus:ring-transparent"},
                m(GithubIcon),
              )
            ),
            m(DarkModeButton, { class: "text-zinc-900 bg-transparent enabled:hover:bg-gray-200 dark:bg-transparent dark:text-zinc-300 enabled:hover:dark:bg-zinc-800 focus:ring-transparent dark:focus:ring-transparent"}),
            m(Navbar.Link, { href: "/settings" },
              m(Button, { class: "text-zinc-900 bg-transparent enabled:hover:bg-gray-200 dark:bg-transparent dark:text-zinc-300 enabled:hover:dark:bg-zinc-800 focus:ring-transparent dark:focus:ring-transparent"},
                m(CogIcon),
              ),
            ),
          ),
        ]
      ),
      m("hr", { class: "border-zinc-600 dark:border-zinc-500" }),
    )
  }
}


const Pane = {
  view({ attrs, children }) {
    const enabled = attrs.enabled;

    if (enabled) {
      return m("pane", children);
    }
    else {
      return null;
    }
  }
}


const MainPane = {
  view: function(vnode) {

    return m(Pane, { enabled: (vnode.attrs.pane === 'main'), ...vnode.attrs },
      m(BlocksControlBar),
      m(BlocksPanel),
    );
  }
}


const ManagerPane = {
  view: function(vnode) {
    return m(Pane, { enabled: (vnode.attrs.pane === 'manager'), ...vnode.attrs },
      // TODO: Add Nixie clock serial settings and monitoring
    );
  }
}


const SettingsPane = {
  view: function(vnode) {
    return m(Pane, { enabled: (vnode.attrs.pane === 'settings'), ...vnode.attrs },
      m(SettingsPanels)
    );
  }
}


export const App = {
  oninit: () => {
    MTC.init();
    LTC.init();
    Serial.init();
  },

  oncreate: () => {
    // Running with delay to wait for Midi and LTC init to be finished
    // TODO: Fix this 
    setTimeout(() => {

      let config = Settings.get_blocks_config();
      if (config) {
        ImportBlocks(config);
        m.redraw();
      }

      setTimer();
    }, 1000);

  },

  view: function(vnode) {
    return m("app", { class: "w-screen h-screen" },
      m(Top),
      m(MainPane,      { ...vnode.attrs }),
      // m(ManagerPane,   { ...vnode.attrs }),
      m(SettingsPane,  { ...vnode.attrs }),
      // m(Bottom),
    );
  }
}


const Bottom = {
  view: function(vnode) {
    // const navbar = new Navbar();
    // return m("h1", "My Mithril App");
    // m(Footer, {
    //     children: m(Footer.Title, "Footer"),
    //     }
    //   );
    // return m("div", { class: "flex" },
    return m(Footer, {class: "fixed bottom-0 left-0 list-none" },
        [
          m(Footer.Title, {title: "Title"}),
          m(Footer.Link, {href: "https://example.com"}, "Click me")
        ]
      )
    // );
  }
}
