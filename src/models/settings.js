import { localStore } from "../lib/storage";
import { MTC } from "../lib/mtc";
import { LTC } from "../lib/ltc";

import { ExportBlocks, ImportBlocks } from "../blocks";


let framerate_store = localStore("framerate");
let display_store = localStore("display_mode");
let labels_store = localStore("labels");
let midi_store = localStore("midi");
let audio_store = localStore("audio");

let blocks_config = localStore("blocks");


export const Settings = {

  default_framerate: framerate_store.get() ? framerate_store.get() : 30,
  set_framerate: function(fps) {
    this.default_framerate = fps;
    framerate_store.set(fps);
  },

  // Blocks
  set_blocks_config: function(config) {
    config = JSON.stringify(config);
    blocks_config.set(config);
  },
  get_blocks_config: function() {
    let config = blocks_config.get();
    config = JSON.parse(config);
    return config;
  },

  // Blocks display
  display_mode: display_store.get() ? display_store.get() : "nodes",
  set_display_mode: function(mode) {
    this.display_mode = mode;
    display_store.set(mode);
  },

  show_labels: labels_store.get() ? labels_store.get() : true,
  set_labels: function(value) {
    this.show_labels = value;
    labels_store.set(value);
  },

  // IO control
  midi_enabled: midi_store.get() ? midi_store.get() : false,
  set_midi: function(value) {
    this.midi_enabled = value;
    midi_store.set(value);
  },
  enable_midi: function() {
    this.set_midi(true);
    MTC.init();
  },
  disable_midi: function() {
    this.set_midi(false);
    MTC.disable();
  },

  audio_enabled: audio_store.get() ? audio_store.get() : false,
  set_audio: function(value) {
    this.audio_enabled = value;
    audio_store.set(value);
  },
  enable_audio: function() {
    this.set_audio(true);
    LTC.init();
  },
  disable_audio: function() {
    this.set_audio(false);
    LTC.disable();
  },

  // Theme
  // theme: "cyan",
  // set_theme: function(theme) {
  //   console.log("Setting theme:", theme)
  //   this.theme = theme
  // },
}